# install docker

apt update

apt install curl -y

curl -fsSL https://get.docker.com | sh

usermod -aG docker ${USER}

# install nodejs and measuredockerbuild
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.37.0/install.sh | bash

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
nvm install --lts

npm i -g measuredockerbuild

# test if docker works

su - ${USER}